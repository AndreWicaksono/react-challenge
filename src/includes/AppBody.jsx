import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';

import CardComponent from './components/Card';

import axios from 'axios';

class ProductList extends Component {
    constructor(props) {
        super(props);
        this.state = {
          products: []
        }
      }

    componentDidMount() {
        axios.get('http://reduxblog.herokuapp.com/api/posts?key=andrew')
          .then(res => {
            // const content = res.data.content;
            this.setState({ products: res.data });
          // console.log('test1',content)
          })
    }

  render() {
    console.log('ini data', this.state.products);

    const loopData = this.state.products.map((product, index) => {
        return(
          <Col md='4' key={index}>
          <CardComponent 
            image={product.categories}
            title={product.title}
            harga={product.content}
          />
          </Col>
        ) 
      }   
      );

    return (
        <div>
            <Container>
                <Row className="d-flex">
                    <CardComponent/>
                </Row>
            </Container>
        </div>
    );
  }
}

export default ProductList;