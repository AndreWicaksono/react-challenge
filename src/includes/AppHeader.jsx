import React, { Component } from 'react';
import Navigasi from './components/NavBar';

class AppHeader extends Component {
  render() {
    return (
      <header className="App-Header">
        <Navigasi/>
      </header>
    );
  }
}

export default AppHeader;
