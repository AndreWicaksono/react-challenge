import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

// Pages
import AppIndex from './AppIndex';
import AppProducts from './AppProducts';
import AppAboutUs from './AppAboutUs';

class AppRouter extends Component {
    render() {
        return (
            <BrowserRouter>
            <div>
                <Route exact path="/" component={AppIndex}/>
                <Route path="/produk" component={AppProducts}/>
                <Route path="/aboutus" component={AppAboutUs}/>
            </div>
            </BrowserRouter>
        )
    }
}

export default AppRouter;