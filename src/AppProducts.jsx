import React, { Component } from 'react';

// Page Structures
import AppHeader from './includes/AppHeader';
import AppBody from './includes/AppBody';

class AppProducts extends Component {
    render() {
        return (
            <div className="Products">
                <AppHeader/>
                <AppBody/>
            </div>
        );
      }
}

export default AppProducts;