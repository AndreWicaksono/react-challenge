import React, { Component } from 'react';

// Page Structures
import AppHeader from './includes/AppHeader';
import AppBody from './includes/AppBody';

// Page Components
import CarouselComponent from './includes/components/Carousel';
// import CardComponent from './includes/components/Card';

class AppIndex extends Component {
  render() {
    return (
      <div className="App">
        <AppHeader/>
        <CarouselComponent/>
        <AppBody/>
      </div>
    );
  }
}

export default AppIndex;
