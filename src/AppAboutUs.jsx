import React, { Component } from 'react';

// Page Structures
import AppHeader from './includes/AppHeader';

class AppAboutUs extends Component {
    render() {
        return (
            <div className="About">
                <AppHeader/>
                <h1>About</h1>
            </div>
        );
      }
}

export default AppAboutUs;